﻿namespace lv
{
  partial class WCC
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.txtWeight = new System.Windows.Forms.TextBox();
      this.txtCost = new System.Windows.Forms.TextBox();
      this.txtPrice = new System.Windows.Forms.TextBox();
      this.cmbType = new System.Windows.Forms.ComboBox();
      this.lvItem = new System.Windows.Forms.ListView();
      this.chWeight = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.chType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.chCost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.chPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.btnAdd = new System.Windows.Forms.Button();
      this.btnSave = new System.Windows.Forms.Button();
      this.btnLoad = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // txtWeight
      // 
      this.txtWeight.Location = new System.Drawing.Point(12, 28);
      this.txtWeight.Name = "txtWeight";
      this.txtWeight.Size = new System.Drawing.Size(100, 20);
      this.txtWeight.TabIndex = 0;
      this.txtWeight.TextChanged += new System.EventHandler(this.weight_changed);
      // 
      // txtCost
      // 
      this.txtCost.Location = new System.Drawing.Point(12, 107);
      this.txtCost.Name = "txtCost";
      this.txtCost.Size = new System.Drawing.Size(100, 20);
      this.txtCost.TabIndex = 2;
      // 
      // txtPrice
      // 
      this.txtPrice.Location = new System.Drawing.Point(12, 146);
      this.txtPrice.Name = "txtPrice";
      this.txtPrice.Size = new System.Drawing.Size(100, 20);
      this.txtPrice.TabIndex = 3;
      // 
      // cmbType
      // 
      this.cmbType.FormattingEnabled = true;
      this.cmbType.Location = new System.Drawing.Point(12, 67);
      this.cmbType.Name = "cmbType";
      this.cmbType.Size = new System.Drawing.Size(100, 21);
      this.cmbType.TabIndex = 4;
      this.cmbType.SelectedIndexChanged += new System.EventHandler(this.type_changed);
      // 
      // lvItem
      // 
      this.lvItem.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chWeight,
            this.chType,
            this.chCost,
            this.chPrice});
      this.lvItem.Location = new System.Drawing.Point(118, 12);
      this.lvItem.Name = "lvItem";
      this.lvItem.Size = new System.Drawing.Size(254, 241);
      this.lvItem.TabIndex = 5;
      this.lvItem.UseCompatibleStateImageBehavior = false;
      this.lvItem.View = System.Windows.Forms.View.Details;
      // 
      // chWeight
      // 
      this.chWeight.Text = "Weight";
      // 
      // chType
      // 
      this.chType.Text = "Type";
      // 
      // chCost
      // 
      this.chCost.Text = "Cost";
      // 
      // chPrice
      // 
      this.chPrice.Text = "Price";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 12);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(35, 13);
      this.label1.TabIndex = 6;
      this.label1.Text = "label1";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 51);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(35, 13);
      this.label2.TabIndex = 7;
      this.label2.Text = "label2";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 91);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(35, 13);
      this.label3.TabIndex = 8;
      this.label3.Text = "label3";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(12, 130);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(35, 13);
      this.label4.TabIndex = 9;
      this.label4.Text = "label4";
      // 
      // btnAdd
      // 
      this.btnAdd.Location = new System.Drawing.Point(37, 172);
      this.btnAdd.Name = "btnAdd";
      this.btnAdd.Size = new System.Drawing.Size(75, 23);
      this.btnAdd.TabIndex = 10;
      this.btnAdd.Text = "Add";
      this.btnAdd.UseVisualStyleBackColor = true;
      this.btnAdd.Click += new System.EventHandler(this.add_click);
      // 
      // btnSave
      // 
      this.btnSave.Location = new System.Drawing.Point(37, 201);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(75, 23);
      this.btnSave.TabIndex = 11;
      this.btnSave.Text = "Save";
      this.btnSave.UseVisualStyleBackColor = true;
      this.btnSave.Click += new System.EventHandler(this.save_click);
      // 
      // btnLoad
      // 
      this.btnLoad.Location = new System.Drawing.Point(37, 230);
      this.btnLoad.Name = "btnLoad";
      this.btnLoad.Size = new System.Drawing.Size(75, 23);
      this.btnLoad.TabIndex = 12;
      this.btnLoad.Text = "Load";
      this.btnLoad.UseVisualStyleBackColor = true;
      this.btnLoad.Click += new System.EventHandler(this.load_click);
      // 
      // WCC
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(384, 261);
      this.Controls.Add(this.btnLoad);
      this.Controls.Add(this.btnSave);
      this.Controls.Add(this.btnAdd);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.lvItem);
      this.Controls.Add(this.cmbType);
      this.Controls.Add(this.txtPrice);
      this.Controls.Add(this.txtCost);
      this.Controls.Add(this.txtWeight);
      this.Name = "WCC";
      this.Text = "Wilbur Control Conduct";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox txtWeight;
    private System.Windows.Forms.TextBox txtCost;
    private System.Windows.Forms.TextBox txtPrice;
    private System.Windows.Forms.ComboBox cmbType;
    private System.Windows.Forms.ListView lvItem;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Button btnAdd;
    private System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.Button btnLoad;
    private System.Windows.Forms.ColumnHeader chWeight;
    private System.Windows.Forms.ColumnHeader chType;
    private System.Windows.Forms.ColumnHeader chCost;
    private System.Windows.Forms.ColumnHeader chPrice;
  }
}

