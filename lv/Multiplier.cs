﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lv
{
  public class Multiplier
  {
    public decimal MinWeight { get; set; }
    public double Value { get; set; }

    public Multiplier(decimal minw, double value)
    {
      MinWeight = minw;
      Value = value;
    }
  }
}
