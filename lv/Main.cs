﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lv
{
  public partial class WCC : Form
  {
    Type[] type =
    {
      new Type("A", 5000),
      new Type("B", 10000),
      new Type("C", 15000),
      new Type("D", 20000)
    };

    Multiplier[] multiplier =
    {
      new Multiplier(0, 1),
      new Multiplier(50, 1.5),
      new Multiplier(100, 2),
      new Multiplier(200, 2.5)
    };

    public WCC()
    {
      InitializeComponent();
      cmbType.DataSource = type;

    }

    private void save_click(object sender, EventArgs e)
    {

    }

    private void load_click(object sender, EventArgs e)
    {

    }

    private void add_click(object sender, EventArgs e)
    {

    }

    private void type_changed(object sender, EventArgs e)
    {
      update_price();
    }

    private void weight_changed(object sender, EventArgs e)
    {
      update_price();
    }

    private void update_price()
    {
      decimal weight = 0;
      decimal cost = (cmbType.SelectedItem as Type).Cost;

      txtCost.Text = cost.ToString();

      if (Decimal.TryParse(txtWeight.Text, out weight))
      {
        txtPrice.Text = calculate_price(weight, cost).ToString();
      }
    }

    decimal calculate_price(decimal weight, decimal cost)
    {

      return weight * cost;
    }
  }
}
