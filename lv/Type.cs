﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lv
{
  public class Type
  {
    public string Code { get; set; }
    public decimal Cost { get; set; }

    public Type(string code, decimal cost)
    {
      Code = code;
      Cost = cost;
    }

    public override string ToString()
    {
      return Code;
    }
  }
}
