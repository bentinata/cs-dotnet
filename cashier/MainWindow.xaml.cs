﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cashier {
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow:Window {
    public MainWindow() {
      InitializeComponent();

      var conn = new NpgsqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["PostgreSQL"].ConnectionString);
      conn.Open();

      var dataSetItem = new DataSet("item");
      var dataAdapterItem = new NpgsqlDataAdapter();
      dataAdapterItem.SelectCommand = new NpgsqlCommand("SELECT * FROM item", conn);
      dataAdapterItem.Fill(dataSetItem, "item");
      var dataTableItem = dataSetItem.Tables["item"];

      uxItem.ItemsSource = dataTableItem.DefaultView;
    }
  }
}
