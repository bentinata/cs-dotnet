﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace resto {
  public partial class Welcome:Form {
    public Welcome() {
      InitializeComponent();
    }

    private void uiNext_Click(object sender, EventArgs e) {
      new SelectMenu().Show(this);
      Hide();
    }
  }
}
