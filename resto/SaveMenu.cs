﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace resto {
  class SaveMenu {
    public SaveMenu(ListView.ListViewItemCollection items) {
      SaveFileDialog saveFileDialog = new SaveFileDialog();
      saveFileDialog.Filter = "Windows text file (*.txt)|*.txt";
      StringBuilder stringBuilder = new StringBuilder();

      foreach (ListViewItem item in items) {
        stringBuilder.Append(item.SubItems[0].Text);
        stringBuilder.Append("##");
        stringBuilder.Append(item.SubItems[1].Text);
        stringBuilder.Append("##");
        stringBuilder.Append(item.SubItems[2].Text);
        stringBuilder.Append("##");
        stringBuilder.AppendLine(item.SubItems[3].Text);
      }

      if (saveFileDialog.ShowDialog() == DialogResult.OK) {
        System.IO.File.WriteAllText(saveFileDialog.FileName, stringBuilder.ToString());
      }
    }
  }
}
