﻿namespace resto
{
    partial class SelectMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.uiAdd = new System.Windows.Forms.Button();
      this.uiDelete = new System.Windows.Forms.Button();
      this.uiQty = new System.Windows.Forms.NumericUpDown();
      this.uiList = new System.Windows.Forms.ListView();
      this.uiPurchase = new System.Windows.Forms.Button();
      this.uiPrice = new System.Windows.Forms.TextBox();
      this.uvMenu = new System.Windows.Forms.GroupBox();
      this.uvQty = new System.Windows.Forms.Label();
      this.uiMenu = new System.Windows.Forms.ComboBox();
      this.uvPrice = new System.Windows.Forms.Label();
      this.Food = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.Price = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.Qty = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.Total = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      ((System.ComponentModel.ISupportInitialize)(this.uiQty)).BeginInit();
      this.uvMenu.SuspendLayout();
      this.SuspendLayout();
      // 
      // uiAdd
      // 
      this.uiAdd.Location = new System.Drawing.Point(6, 179);
      this.uiAdd.Name = "uiAdd";
      this.uiAdd.Size = new System.Drawing.Size(120, 23);
      this.uiAdd.TabIndex = 0;
      this.uiAdd.Text = "Add";
      this.uiAdd.UseVisualStyleBackColor = true;
      this.uiAdd.Click += new System.EventHandler(this.uiAdd_Click);
      // 
      // uiDelete
      // 
      this.uiDelete.Location = new System.Drawing.Point(6, 208);
      this.uiDelete.Name = "uiDelete";
      this.uiDelete.Size = new System.Drawing.Size(120, 23);
      this.uiDelete.TabIndex = 1;
      this.uiDelete.Text = "Delete";
      this.uiDelete.UseVisualStyleBackColor = true;
      this.uiDelete.Click += new System.EventHandler(this.uiDelete_Click);
      // 
      // uiQty
      // 
      this.uiQty.Location = new System.Drawing.Point(6, 98);
      this.uiQty.Name = "uiQty";
      this.uiQty.Size = new System.Drawing.Size(120, 20);
      this.uiQty.TabIndex = 2;
      // 
      // uiList
      // 
      this.uiList.CheckBoxes = true;
      this.uiList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Food,
            this.Price,
            this.Qty,
            this.Total});
      this.uiList.GridLines = true;
      this.uiList.Location = new System.Drawing.Point(150, 12);
      this.uiList.Name = "uiList";
      this.uiList.Size = new System.Drawing.Size(322, 208);
      this.uiList.TabIndex = 3;
      this.uiList.UseCompatibleStateImageBehavior = false;
      this.uiList.View = System.Windows.Forms.View.Details;
      // 
      // uiPurchase
      // 
      this.uiPurchase.Location = new System.Drawing.Point(150, 226);
      this.uiPurchase.Name = "uiPurchase";
      this.uiPurchase.Size = new System.Drawing.Size(322, 23);
      this.uiPurchase.TabIndex = 4;
      this.uiPurchase.Text = "Purchase";
      this.uiPurchase.UseVisualStyleBackColor = true;
      this.uiPurchase.Click += new System.EventHandler(this.uiPurchase_Click);
      // 
      // uiPrice
      // 
      this.uiPrice.Location = new System.Drawing.Point(6, 59);
      this.uiPrice.Name = "uiPrice";
      this.uiPrice.Size = new System.Drawing.Size(120, 20);
      this.uiPrice.TabIndex = 5;
      // 
      // uvMenu
      // 
      this.uvMenu.Controls.Add(this.uvQty);
      this.uvMenu.Controls.Add(this.uiMenu);
      this.uvMenu.Controls.Add(this.uiDelete);
      this.uvMenu.Controls.Add(this.uvPrice);
      this.uvMenu.Controls.Add(this.uiQty);
      this.uvMenu.Controls.Add(this.uiAdd);
      this.uvMenu.Controls.Add(this.uiPrice);
      this.uvMenu.Location = new System.Drawing.Point(12, 12);
      this.uvMenu.Name = "uvMenu";
      this.uvMenu.Size = new System.Drawing.Size(132, 237);
      this.uvMenu.TabIndex = 7;
      this.uvMenu.TabStop = false;
      this.uvMenu.Text = "Menu";
      // 
      // uvQty
      // 
      this.uvQty.AutoSize = true;
      this.uvQty.Location = new System.Drawing.Point(6, 82);
      this.uvQty.Name = "uvQty";
      this.uvQty.Size = new System.Drawing.Size(46, 13);
      this.uvQty.TabIndex = 9;
      this.uvQty.Text = "Quantity";
      // 
      // uiMenu
      // 
      this.uiMenu.FormattingEnabled = true;
      this.uiMenu.Location = new System.Drawing.Point(6, 19);
      this.uiMenu.Name = "uiMenu";
      this.uiMenu.Size = new System.Drawing.Size(120, 21);
      this.uiMenu.TabIndex = 7;
      this.uiMenu.SelectedIndexChanged += new System.EventHandler(this.uiMenu_SelectedIndexChanged);
      // 
      // uvPrice
      // 
      this.uvPrice.AutoSize = true;
      this.uvPrice.Location = new System.Drawing.Point(6, 43);
      this.uvPrice.Name = "uvPrice";
      this.uvPrice.Size = new System.Drawing.Size(31, 13);
      this.uvPrice.TabIndex = 8;
      this.uvPrice.Text = "Price";
      // 
      // Food
      // 
      this.Food.Text = "Food";
      this.Food.Width = 120;
      // 
      // Price
      // 
      this.Price.Text = "Price";
      this.Price.Width = 120;
      // 
      // Qty
      // 
      this.Qty.Text = "Qty";
      this.Qty.Width = 30;
      // 
      // Total
      // 
      this.Total.Text = "Total";
      // 
      // SelectMenu
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(484, 261);
      this.Controls.Add(this.uvMenu);
      this.Controls.Add(this.uiPurchase);
      this.Controls.Add(this.uiList);
      this.Name = "SelectMenu";
      this.Text = "Select Menu";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SelectMenu_FormClosing);
      ((System.ComponentModel.ISupportInitialize)(this.uiQty)).EndInit();
      this.uvMenu.ResumeLayout(false);
      this.uvMenu.PerformLayout();
      this.ResumeLayout(false);

        }

    #endregion

    private System.Windows.Forms.Button uiAdd;
    private System.Windows.Forms.Button uiDelete;
    private System.Windows.Forms.NumericUpDown uiQty;
    private System.Windows.Forms.ListView uiList;
    private System.Windows.Forms.Button uiPurchase;
    private System.Windows.Forms.TextBox uiPrice;
    private System.Windows.Forms.GroupBox uvMenu;
    private System.Windows.Forms.Label uvQty;
    private System.Windows.Forms.ComboBox uiMenu;
    private System.Windows.Forms.Label uvPrice;
    private System.Windows.Forms.ColumnHeader Food;
    private System.Windows.Forms.ColumnHeader Price;
    private System.Windows.Forms.ColumnHeader Qty;
    private System.Windows.Forms.ColumnHeader Total;
  }
}

