﻿namespace resto {
  partial class Welcome {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.uvWelcome = new System.Windows.Forms.Label();
      this.uiNext = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // uvWelcome
      // 
      this.uvWelcome.Dock = System.Windows.Forms.DockStyle.Fill;
      this.uvWelcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uvWelcome.Location = new System.Drawing.Point(0, 0);
      this.uvWelcome.Name = "uvWelcome";
      this.uvWelcome.Size = new System.Drawing.Size(284, 111);
      this.uvWelcome.TabIndex = 0;
      this.uvWelcome.Text = "Welcome to Provis Resto";
      this.uvWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // uiNext
      // 
      this.uiNext.Location = new System.Drawing.Point(197, 76);
      this.uiNext.Name = "uiNext";
      this.uiNext.Size = new System.Drawing.Size(75, 23);
      this.uiNext.TabIndex = 1;
      this.uiNext.Text = "Next";
      this.uiNext.UseVisualStyleBackColor = true;
      this.uiNext.Click += new System.EventHandler(this.uiNext_Click);
      // 
      // Welcome
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(284, 111);
      this.Controls.Add(this.uiNext);
      this.Controls.Add(this.uvWelcome);
      this.Name = "Welcome";
      this.Text = "Welcome";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label uvWelcome;
    private System.Windows.Forms.Button uiNext;
  }
}