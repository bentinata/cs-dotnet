﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace resto {
  public partial class SelectMenu:Form {
    public SelectMenu() {
      InitializeComponent();

      uiList.Columns[uiList.Columns.Count - 1].Width = -2;
      uiMenu.DataSource = new List<Menu>{
        new Menu("Mie Kuah Java", 17000),
        new Menu("Mie Kuah PHP", 13000),
        new Menu("Mie Goreng HTML", 10000),
        new Menu("Nasi Goreng Python", 12000),
        new Menu("Bubur R", 15000), // Haha, R in Ilkom UPI. Such joke.
        new Menu("Bubur XML", 8000),
        new Menu("Es Teh C#", 9000),
        new Menu("Es Teh C++", 5000)
      };
    }

    private void SelectMenu_FormClosing(object sender, FormClosingEventArgs e) {
      Application.Exit();
    }

    private void uiMenu_SelectedIndexChanged(object sender, EventArgs e) {
      uiPrice.Text = (uiMenu.SelectedItem as Menu).Price.ToString();
    }

    private void uiAdd_Click(object sender, EventArgs e) {
      if (uiQty.Value == 0) {
        MessageBox.Show("Add quantity!");
        return;
      }

      uiList.Items.Add((uiMenu.SelectedItem as Menu).ToListViewItem(uiQty.Value));
    }

    private void uiDelete_Click(object sender, EventArgs e) {
      if (uiList.Items.Count == 0) {
        MessageBox.Show("You don't have menu list");
        return;
      }

      if (uiList.CheckedItems.Count == 0) {
        MessageBox.Show("Checklist menu!");
        return;
      }

      foreach (ListViewItem item in uiList.CheckedItems) {
        item.Remove();
      }
    }

    private void uiPurchase_Click(object sender, EventArgs e) {
      if (uiList.Items.Count == 0) {
        MessageBox.Show("You don't have menu list");
        return;
      }

      new SaveMenu(uiList.Items);
    }
  }

  class Menu {
    public string Food { get; set; }
    public decimal Price { get; set; }

    public Menu(string food, decimal price) {
      Food = food;
      Price = price;
    }

    public override string ToString() {
      return Food;
    }

    public ListViewItem ToListViewItem(decimal qty) {
      return new ListViewItem(new[] {
        Food,
        Price.ToString(),
        qty.ToString(),
        (qty * Price).ToString()
      });
    }
  }
}
