﻿namespace _1b
{
  partial class RentalApp
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cmbMobil = new System.Windows.Forms.ComboBox();
      this.dateTanggalPinjam = new System.Windows.Forms.DateTimePicker();
      this.dateJamPinjam = new System.Windows.Forms.DateTimePicker();
      this.dateTanggalKembali = new System.Windows.Forms.DateTimePicker();
      this.dateJamKembali = new System.Windows.Forms.DateTimePicker();
      this.txtBiayaPerJam = new System.Windows.Forms.TextBox();
      this.txtDurasi = new System.Windows.Forms.TextBox();
      this.txtBiaya = new System.Windows.Forms.TextBox();
      this.btnHitung = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // cmbMobil
      // 
      this.cmbMobil.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbMobil.FormattingEnabled = true;
      this.cmbMobil.Location = new System.Drawing.Point(242, 63);
      this.cmbMobil.Name = "cmbMobil";
      this.cmbMobil.Size = new System.Drawing.Size(121, 21);
      this.cmbMobil.TabIndex = 0;
      this.cmbMobil.SelectedIndexChanged += new System.EventHandler(this.mobil_changed);
      // 
      // dateTanggalPinjam
      // 
      this.dateTanggalPinjam.Format = System.Windows.Forms.DateTimePickerFormat.Short;
      this.dateTanggalPinjam.Location = new System.Drawing.Point(182, 116);
      this.dateTanggalPinjam.Name = "dateTanggalPinjam";
      this.dateTanggalPinjam.Size = new System.Drawing.Size(100, 20);
      this.dateTanggalPinjam.TabIndex = 1;
      this.dateTanggalPinjam.ValueChanged += new System.EventHandler(this.date_changed);
      // 
      // dateJamPinjam
      // 
      this.dateJamPinjam.CustomFormat = "HH:mm";
      this.dateJamPinjam.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dateJamPinjam.Location = new System.Drawing.Point(288, 116);
      this.dateJamPinjam.Name = "dateJamPinjam";
      this.dateJamPinjam.ShowUpDown = true;
      this.dateJamPinjam.Size = new System.Drawing.Size(75, 20);
      this.dateJamPinjam.TabIndex = 2;
      this.dateJamPinjam.ValueChanged += new System.EventHandler(this.date_changed);
      // 
      // dateTanggalKembali
      // 
      this.dateTanggalKembali.Format = System.Windows.Forms.DateTimePickerFormat.Short;
      this.dateTanggalKembali.Location = new System.Drawing.Point(182, 142);
      this.dateTanggalKembali.Name = "dateTanggalKembali";
      this.dateTanggalKembali.Size = new System.Drawing.Size(100, 20);
      this.dateTanggalKembali.TabIndex = 3;
      this.dateTanggalKembali.ValueChanged += new System.EventHandler(this.date_changed);
      // 
      // dateJamKembali
      // 
      this.dateJamKembali.CustomFormat = "HH:mm";
      this.dateJamKembali.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dateJamKembali.Location = new System.Drawing.Point(288, 142);
      this.dateJamKembali.Name = "dateJamKembali";
      this.dateJamKembali.ShowUpDown = true;
      this.dateJamKembali.Size = new System.Drawing.Size(75, 20);
      this.dateJamKembali.TabIndex = 4;
      this.dateJamKembali.ValueChanged += new System.EventHandler(this.date_changed);
      // 
      // txtBiayaPerJam
      // 
      this.txtBiayaPerJam.Location = new System.Drawing.Point(263, 90);
      this.txtBiayaPerJam.Name = "txtBiayaPerJam";
      this.txtBiayaPerJam.ReadOnly = true;
      this.txtBiayaPerJam.Size = new System.Drawing.Size(100, 20);
      this.txtBiayaPerJam.TabIndex = 5;
      // 
      // txtDurasi
      // 
      this.txtDurasi.Location = new System.Drawing.Point(263, 168);
      this.txtDurasi.Name = "txtDurasi";
      this.txtDurasi.ReadOnly = true;
      this.txtDurasi.Size = new System.Drawing.Size(100, 20);
      this.txtDurasi.TabIndex = 6;
      // 
      // txtBiaya
      // 
      this.txtBiaya.Location = new System.Drawing.Point(263, 227);
      this.txtBiaya.Name = "txtBiaya";
      this.txtBiaya.ReadOnly = true;
      this.txtBiaya.Size = new System.Drawing.Size(100, 20);
      this.txtBiaya.TabIndex = 7;
      // 
      // btnHitung
      // 
      this.btnHitung.Location = new System.Drawing.Point(288, 198);
      this.btnHitung.Name = "btnHitung";
      this.btnHitung.Size = new System.Drawing.Size(75, 23);
      this.btnHitung.TabIndex = 8;
      this.btnHitung.Text = "Hitung";
      this.btnHitung.UseVisualStyleBackColor = true;
      this.btnHitung.Click += new System.EventHandler(this.hitung_click);
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(16, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(347, 51);
      this.label1.TabIndex = 9;
      this.label1.Text = "Rental Mobil GIK";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 66);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(65, 13);
      this.label2.TabIndex = 10;
      this.label2.Text = "Pilihan mobil";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 122);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(72, 13);
      this.label3.TabIndex = 11;
      this.label3.Text = "Waktu pinjam";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(12, 148);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(78, 13);
      this.label4.TabIndex = 12;
      this.label4.Text = "Waktu kembali";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(12, 93);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(70, 13);
      this.label5.TabIndex = 13;
      this.label5.Text = "Biaya per jam";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(12, 171);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(62, 13);
      this.label6.TabIndex = 14;
      this.label6.Text = "Durasi (jam)";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(12, 230);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(59, 13);
      this.label7.TabIndex = 15;
      this.label7.Text = "Total biaya";
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(384, 261);
      this.Controls.Add(this.label7);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.btnHitung);
      this.Controls.Add(this.txtBiaya);
      this.Controls.Add(this.txtDurasi);
      this.Controls.Add(this.txtBiayaPerJam);
      this.Controls.Add(this.dateJamKembali);
      this.Controls.Add(this.dateTanggalKembali);
      this.Controls.Add(this.dateJamPinjam);
      this.Controls.Add(this.dateTanggalPinjam);
      this.Controls.Add(this.cmbMobil);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Name = "Form1";
      this.Text = "Rental App";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ComboBox cmbMobil;
    private System.Windows.Forms.DateTimePicker dateTanggalPinjam;
    private System.Windows.Forms.DateTimePicker dateJamPinjam;
    private System.Windows.Forms.DateTimePicker dateTanggalKembali;
    private System.Windows.Forms.DateTimePicker dateJamKembali;
    private System.Windows.Forms.TextBox txtBiayaPerJam;
    private System.Windows.Forms.TextBox txtDurasi;
    private System.Windows.Forms.TextBox txtBiaya;
    private System.Windows.Forms.Button btnHitung;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label7;
  }
}

