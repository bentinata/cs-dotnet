﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace _1b
{
  public partial class RentalApp : Form
  {
    Mobil[] mobilMobil =
    {
      new Mobil("Avanza", 400000),
      new Mobil("Xenia", 400000),
      new Mobil("APV", 450000),
      new Mobil("Innova", 500000),
      new Mobil("Alphard", 1000000)
    };

    // Dirty hacks, faster and easier than deattaching-reattaching events tho.
    private bool skip = false;

    public RentalApp()
    {
      InitializeComponent();
      date_changed(this, null);
      foreach(Mobil mobil in mobilMobil)
      {
        cmbMobil.Items.Add(mobil);
      }
    }

    private void hitung_click(object sender, EventArgs e)
    {
      int perjam = 0;
      int durasi = 0;

      int.TryParse(txtBiayaPerJam.Text, out perjam);
      int.TryParse(txtDurasi.Text, out durasi);

      txtBiaya.Text = String.Format(new CultureInfo("id"), "{0:C}", perjam * durasi);
    }

    private void date_changed(object sender, EventArgs e)
    {
      if (skip) return;

      DateTime pinjam = dateTanggalPinjam.Value.Date.AddHours(dateJamPinjam.Value.TimeOfDay.Hours);
      DateTime kembali = dateTanggalKembali.Value.Date.AddHours(dateJamKembali.Value.TimeOfDay.Hours);
      var durasi = (kembali - pinjam).TotalHours;

      if (durasi < 0)
      {
        MessageBox.Show("Tanggal yang anda masukkan tidak valid.");
        skip = true;
        reset_date();
        skip = false;
      }
      else
        txtDurasi.Text = durasi.ToString();
    }

    void reset_date()
    {
      Action<Control.ControlCollection> reset = null;

      (reset = controls =>
      {
        foreach (Control control in controls)
        {
          if (control is DateTimePicker)
            (control as DateTimePicker).Value = DateTime.Now;
          else
            reset(control.Controls);
        }
      })(Controls);
    }

    private void mobil_changed(object sender, EventArgs e)
    {
      txtBiayaPerJam.Text = (cmbMobil.SelectedItem as Mobil).Harga.ToString();
    }
  }
}
