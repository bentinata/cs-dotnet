﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1b
{
  public class Mobil
  {
    public string Nama { get; set; }
    public int Harga { get; set; }

    public Mobil(string nama, int harga)
    {
      Nama = nama;
      Harga = harga;
    }

    public override string ToString()
    {
      return Nama;
    }
  }
}
