﻿namespace _1a
{
    partial class SimpleCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.txtVariable1 = new System.Windows.Forms.TextBox();
      this.txtVariable2 = new System.Windows.Forms.TextBox();
      this.btnAdd = new System.Windows.Forms.Button();
      this.btnSubtract = new System.Windows.Forms.Button();
      this.btnMultiply = new System.Windows.Forms.Button();
      this.btnDivide = new System.Windows.Forms.Button();
      this.txtReset = new System.Windows.Forms.Button();
      this.txtNext = new System.Windows.Forms.Button();
      this.txtResult = new System.Windows.Forms.TextBox();
      this.SuspendLayout();
      // 
      // txtVariable1
      // 
      this.txtVariable1.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtVariable1.Location = new System.Drawing.Point(12, 12);
      this.txtVariable1.Name = "txtVariable1";
      this.txtVariable1.Size = new System.Drawing.Size(110, 56);
      this.txtVariable1.TabIndex = 0;
      // 
      // txtVariable2
      // 
      this.txtVariable2.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtVariable2.Location = new System.Drawing.Point(12, 74);
      this.txtVariable2.Name = "txtVariable2";
      this.txtVariable2.Size = new System.Drawing.Size(110, 56);
      this.txtVariable2.TabIndex = 1;
      // 
      // btnAdd
      // 
      this.btnAdd.BackColor = System.Drawing.Color.LightSeaGreen;
      this.btnAdd.Location = new System.Drawing.Point(12, 136);
      this.btnAdd.Name = "btnAdd";
      this.btnAdd.Size = new System.Drawing.Size(23, 23);
      this.btnAdd.TabIndex = 2;
      this.btnAdd.Text = "+";
      this.btnAdd.UseVisualStyleBackColor = false;
      this.btnAdd.Click += new System.EventHandler(this.op_click);
      // 
      // btnSubtract
      // 
      this.btnSubtract.BackColor = System.Drawing.Color.LightSeaGreen;
      this.btnSubtract.Location = new System.Drawing.Point(41, 136);
      this.btnSubtract.Name = "btnSubtract";
      this.btnSubtract.Size = new System.Drawing.Size(23, 23);
      this.btnSubtract.TabIndex = 3;
      this.btnSubtract.Text = "−";
      this.btnSubtract.UseVisualStyleBackColor = false;
      this.btnSubtract.Click += new System.EventHandler(this.op_click);
      // 
      // btnMultiply
      // 
      this.btnMultiply.BackColor = System.Drawing.Color.LightSeaGreen;
      this.btnMultiply.Location = new System.Drawing.Point(70, 136);
      this.btnMultiply.Name = "btnMultiply";
      this.btnMultiply.Size = new System.Drawing.Size(23, 23);
      this.btnMultiply.TabIndex = 4;
      this.btnMultiply.Text = "×";
      this.btnMultiply.UseVisualStyleBackColor = false;
      this.btnMultiply.Click += new System.EventHandler(this.op_click);
      // 
      // btnDivide
      // 
      this.btnDivide.BackColor = System.Drawing.Color.LightSeaGreen;
      this.btnDivide.Location = new System.Drawing.Point(99, 136);
      this.btnDivide.Name = "btnDivide";
      this.btnDivide.Size = new System.Drawing.Size(23, 23);
      this.btnDivide.TabIndex = 5;
      this.btnDivide.Text = "÷";
      this.btnDivide.UseVisualStyleBackColor = false;
      this.btnDivide.Click += new System.EventHandler(this.op_click);
      // 
      // txtReset
      // 
      this.txtReset.BackColor = System.Drawing.Color.LightSeaGreen;
      this.txtReset.Location = new System.Drawing.Point(162, 136);
      this.txtReset.Name = "txtReset";
      this.txtReset.Size = new System.Drawing.Size(52, 23);
      this.txtReset.TabIndex = 6;
      this.txtReset.Text = "Reset";
      this.txtReset.UseVisualStyleBackColor = false;
      this.txtReset.Click += new System.EventHandler(this.reset_click);
      // 
      // txtNext
      // 
      this.txtNext.BackColor = System.Drawing.Color.LightSeaGreen;
      this.txtNext.Location = new System.Drawing.Point(220, 136);
      this.txtNext.Name = "txtNext";
      this.txtNext.Size = new System.Drawing.Size(52, 23);
      this.txtNext.TabIndex = 7;
      this.txtNext.Text = "Next";
      this.txtNext.UseVisualStyleBackColor = false;
      this.txtNext.Click += new System.EventHandler(this.next_click);
      // 
      // txtResult
      // 
      this.txtResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtResult.Location = new System.Drawing.Point(162, 74);
      this.txtResult.Name = "txtResult";
      this.txtResult.ReadOnly = true;
      this.txtResult.Size = new System.Drawing.Size(110, 56);
      this.txtResult.TabIndex = 8;
      // 
      // SimpleCalculator
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(284, 171);
      this.Controls.Add(this.txtResult);
      this.Controls.Add(this.txtNext);
      this.Controls.Add(this.txtReset);
      this.Controls.Add(this.btnDivide);
      this.Controls.Add(this.btnMultiply);
      this.Controls.Add(this.btnSubtract);
      this.Controls.Add(this.btnAdd);
      this.Controls.Add(this.txtVariable2);
      this.Controls.Add(this.txtVariable1);
      this.Name = "SimpleCalculator";
      this.Text = "Form1";
      this.Load += new System.EventHandler(this.SimpleCalculator_load);
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtVariable1;
        private System.Windows.Forms.TextBox txtVariable2;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnSubtract;
        private System.Windows.Forms.Button btnMultiply;
        private System.Windows.Forms.Button btnDivide;
        private System.Windows.Forms.Button txtReset;
        private System.Windows.Forms.Button txtNext;
        private System.Windows.Forms.TextBox txtResult;
    }
}

