﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace _1a
{
  public partial class SimpleCalculator : Form
  {
    public SimpleCalculator()
    {
      InitializeComponent();
    }

    private void SimpleCalculator_load(object sender, EventArgs e)
    {

    }

    private void op_click(object sender, EventArgs e)
    {
      MethodInfo opfn = this.GetType().GetMethod((sender as Button).Name.ToLower().Replace("btn", ""));
      float v1, v2;

      if (!float.TryParse(txtVariable1.Text, out v1) || !float.TryParse(txtVariable2.Text, out v2) || v2 == 0)
        MessageBox.Show("Invalid input!");
      else
        txtResult.Text = opfn.Invoke(this, new object[] { v1, v2 }).ToString();
    }

    public static float add(float a, float b)
    {
      return a + b;
    }

    public static float subtract(float a, float b)
    {
      return a - b;
    }

    public static float multiply(float a, float b)
    {
      return a * b;
    }

    public static float divide(float a, float b)
    {
      return a / b;
    }

    private void reset_click(object sender, EventArgs e)
    {
      Action<Control.ControlCollection> clear = null;

      (clear = controls =>
      {
        foreach (Control control in controls)
        {
          if (control is TextBox)
            (control as TextBox).Clear();
          else
            clear(control.Controls);
        }

      })(Controls);
    }

    private void next_click(object sender, EventArgs e)
    {
      txtVariable1.Text = txtResult.Text;
      txtVariable2.Clear();
      txtResult.Clear();
    }
  }
}
