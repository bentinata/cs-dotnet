﻿namespace parkir
{
  partial class frmStruk
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.txtNoParkir = new System.Windows.Forms.TextBox();
      this.txtTanggal = new System.Windows.Forms.TextBox();
      this.txtJam = new System.Windows.Forms.TextBox();
      this.timerClose = new System.Windows.Forms.Timer(this.components);
      this.txtKode = new System.Windows.Forms.TextBox();
      this.txtJenis = new System.Windows.Forms.TextBox();
      this.picJenis = new System.Windows.Forms.PictureBox();
      this.printDocument = new System.Drawing.Printing.PrintDocument();
      this.timerPrint = new System.Windows.Forms.Timer(this.components);
      ((System.ComponentModel.ISupportInitialize)(this.picJenis)).BeginInit();
      this.SuspendLayout();
      // 
      // txtNoParkir
      // 
      this.txtNoParkir.BackColor = System.Drawing.Color.White;
      this.txtNoParkir.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtNoParkir.Location = new System.Drawing.Point(12, 68);
      this.txtNoParkir.Name = "txtNoParkir";
      this.txtNoParkir.ReadOnly = true;
      this.txtNoParkir.Size = new System.Drawing.Size(160, 13);
      this.txtNoParkir.TabIndex = 10;
      this.txtNoParkir.Text = "001";
      this.txtNoParkir.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // txtTanggal
      // 
      this.txtTanggal.BackColor = System.Drawing.Color.White;
      this.txtTanggal.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtTanggal.Location = new System.Drawing.Point(12, 168);
      this.txtTanggal.Name = "txtTanggal";
      this.txtTanggal.ReadOnly = true;
      this.txtTanggal.Size = new System.Drawing.Size(160, 13);
      this.txtTanggal.TabIndex = 21;
      this.txtTanggal.Text = "2017-01-01";
      // 
      // txtJam
      // 
      this.txtJam.BackColor = System.Drawing.Color.White;
      this.txtJam.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtJam.Location = new System.Drawing.Point(12, 187);
      this.txtJam.Name = "txtJam";
      this.txtJam.ReadOnly = true;
      this.txtJam.Size = new System.Drawing.Size(160, 13);
      this.txtJam.TabIndex = 23;
      this.txtJam.Text = "08:00";
      // 
      // timerClose
      // 
      this.timerClose.Enabled = true;
      this.timerClose.Interval = 15000;
      this.timerClose.Tick += new System.EventHandler(this.struk_close);
      // 
      // txtKode
      // 
      this.txtKode.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtKode.Font = new System.Drawing.Font("Consolas", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtKode.Location = new System.Drawing.Point(12, 87);
      this.txtKode.Name = "txtKode";
      this.txtKode.Size = new System.Drawing.Size(160, 75);
      this.txtKode.TabIndex = 24;
      this.txtKode.Text = "CODE";
      this.txtKode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // txtJenis
      // 
      this.txtJenis.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.txtJenis.BackColor = System.Drawing.Color.White;
      this.txtJenis.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtJenis.Location = new System.Drawing.Point(72, 12);
      this.txtJenis.Name = "txtJenis";
      this.txtJenis.Size = new System.Drawing.Size(100, 13);
      this.txtJenis.TabIndex = 25;
      this.txtJenis.Text = "Parkir Mobil";
      this.txtJenis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // picJenis
      // 
      this.picJenis.Location = new System.Drawing.Point(12, 12);
      this.picJenis.Name = "picJenis";
      this.picJenis.Size = new System.Drawing.Size(50, 50);
      this.picJenis.TabIndex = 26;
      this.picJenis.TabStop = false;
      // 
      // printDocument
      // 
      this.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument_PrintPage);
      // 
      // timerPrint
      // 
      this.timerPrint.Enabled = true;
      this.timerPrint.Tick += new System.EventHandler(this.print_form);
      // 
      // frmStruk
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.White;
      this.ClientSize = new System.Drawing.Size(184, 261);
      this.Controls.Add(this.picJenis);
      this.Controls.Add(this.txtJenis);
      this.Controls.Add(this.txtKode);
      this.Controls.Add(this.txtJam);
      this.Controls.Add(this.txtTanggal);
      this.Controls.Add(this.txtNoParkir);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Name = "frmStruk";
      this.Text = "Terima Kasih";
      this.Click += new System.EventHandler(this.struk_close);
      ((System.ComponentModel.ISupportInitialize)(this.picJenis)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion
    private System.Windows.Forms.TextBox txtNoParkir;
    private System.Windows.Forms.TextBox txtTanggal;
    private System.Windows.Forms.TextBox txtJam;
    private System.Windows.Forms.Timer timerClose;
    private System.Windows.Forms.TextBox txtKode;
    private System.Windows.Forms.TextBox txtJenis;
    private System.Windows.Forms.PictureBox picJenis;
    private System.Drawing.Printing.PrintDocument printDocument;
    private System.Windows.Forms.Timer timerPrint;
  }
}