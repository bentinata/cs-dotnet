﻿namespace parkir
{
    partial class frmPilih
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.label1 = new System.Windows.Forms.Label();
      this.btnMasuk = new System.Windows.Forms.Button();
      this.btnKeluar = new System.Windows.Forms.Button();
      this.btnLogout = new System.Windows.Forms.Button();
      this.txtUser = new System.Windows.Forms.TextBox();
      this.txtJabatan = new System.Windows.Forms.TextBox();
      this.btnLaporan = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = global::parkir.Properties.Resources.person;
      this.pictureBox1.Location = new System.Drawing.Point(16, 12);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(60, 60);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pictureBox1.TabIndex = 0;
      this.pictureBox1.TabStop = false;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(61, 155);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(107, 16);
      this.label1.TabIndex = 1;
      this.label1.Text = "Pilih Aplikasi :";
      // 
      // btnMasuk
      // 
      this.btnMasuk.BackColor = System.Drawing.Color.Pink;
      this.btnMasuk.Location = new System.Drawing.Point(95, 187);
      this.btnMasuk.Name = "btnMasuk";
      this.btnMasuk.Size = new System.Drawing.Size(135, 45);
      this.btnMasuk.TabIndex = 2;
      this.btnMasuk.Text = "&MASUK";
      this.btnMasuk.UseVisualStyleBackColor = false;
      this.btnMasuk.Click += new System.EventHandler(this.btnMasuk_Click);
      // 
      // btnKeluar
      // 
      this.btnKeluar.BackColor = System.Drawing.Color.Plum;
      this.btnKeluar.Location = new System.Drawing.Point(95, 234);
      this.btnKeluar.Name = "btnKeluar";
      this.btnKeluar.Size = new System.Drawing.Size(135, 45);
      this.btnKeluar.TabIndex = 3;
      this.btnKeluar.Text = "&KELUAR";
      this.btnKeluar.UseVisualStyleBackColor = false;
      this.btnKeluar.Click += new System.EventHandler(this.btnKeluar_Click);
      // 
      // btnLogout
      // 
      this.btnLogout.BackColor = System.Drawing.Color.Gainsboro;
      this.btnLogout.Location = new System.Drawing.Point(221, 346);
      this.btnLogout.Name = "btnLogout";
      this.btnLogout.Size = new System.Drawing.Size(75, 23);
      this.btnLogout.TabIndex = 4;
      this.btnLogout.Text = "&Logout";
      this.btnLogout.UseVisualStyleBackColor = false;
      this.btnLogout.Click += new System.EventHandler(this.button3_Click);
      // 
      // txtUser
      // 
      this.txtUser.BackColor = System.Drawing.Color.SkyBlue;
      this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtUser.Location = new System.Drawing.Point(16, 78);
      this.txtUser.Name = "txtUser";
      this.txtUser.Size = new System.Drawing.Size(100, 13);
      this.txtUser.TabIndex = 5;
      this.txtUser.Text = "Nama User";
      // 
      // txtJabatan
      // 
      this.txtJabatan.BackColor = System.Drawing.Color.SkyBlue;
      this.txtJabatan.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtJabatan.Location = new System.Drawing.Point(16, 94);
      this.txtJabatan.Name = "txtJabatan";
      this.txtJabatan.Size = new System.Drawing.Size(100, 13);
      this.txtJabatan.TabIndex = 6;
      this.txtJabatan.Text = "Jabatan";
      // 
      // btnLaporan
      // 
      this.btnLaporan.Location = new System.Drawing.Point(95, 285);
      this.btnLaporan.Name = "btnLaporan";
      this.btnLaporan.Size = new System.Drawing.Size(135, 45);
      this.btnLaporan.TabIndex = 7;
      this.btnLaporan.Text = "&LAPORAN";
      this.btnLaporan.UseVisualStyleBackColor = true;
      // 
      // frmPilih
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.SkyBlue;
      this.ClientSize = new System.Drawing.Size(320, 390);
      this.Controls.Add(this.btnLaporan);
      this.Controls.Add(this.txtJabatan);
      this.Controls.Add(this.txtUser);
      this.Controls.Add(this.btnLogout);
      this.Controls.Add(this.btnKeluar);
      this.Controls.Add(this.btnMasuk);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.pictureBox1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Name = "frmPilih";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Form1";
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnMasuk;
        private System.Windows.Forms.Button btnKeluar;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.TextBox txtJabatan;
    private System.Windows.Forms.Button btnLaporan;
  }
}