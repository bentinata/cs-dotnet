﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace parkir
{
  public partial class frmMasuk : Form
  {
    private string Kendaraan;

    public frmMasuk(string kendaraan)
    {
      Kendaraan = kendaraan;
      InitializeComponent();

      txtTanggal.Text = DateTime.Now.ToString("yyyy-MM-dd");
      txtJenisKendaraan.Text = kendaraan;
    }

    private void btnMasuk_Click(object sender, EventArgs e)
    {
      int nomor = (Owner as frmAwal).add_jumlah();
      Form struk = new frmStruk(nomor, DateTime.Now, Kendaraan);
      struk.Show(this);
      Hide();
    }

    private void tick(object sender, EventArgs e)
    {
      txtWaktu.Text = DateTime.Now.TimeOfDay.ToString();
    }

    private void show_parent(object sender, FormClosedEventArgs e)
    {
      Owner.Show();
    }

        private void frmMasuk_Load(object sender, EventArgs e)
        {

        }
    }
}
