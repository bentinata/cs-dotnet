﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parkir
{
  class Encoder
  {
    private string Chars { get; set; }
    private int Pad { get; set; }

    public Encoder(string chars, int pad = 0)
    {
      Chars = chars;
      Pad = pad;
    }

    public string Encode(int data)
    {
      string encoded = "";

      while (data != 0)
      {
        encoded = Chars[data % Chars.Length] + encoded;
        data = data / Chars.Length;
      }

      int pad = Pad - encoded.Length;
      
      while (pad > 0)
      {
        encoded = Chars[0] + encoded;
        pad = pad - 1;
      }

      return encoded;
    }
  }
}
