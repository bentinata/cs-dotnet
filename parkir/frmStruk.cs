﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace parkir
{
  public partial class frmStruk : Form
  {
    private Encoder Base36 = new Encoder("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 4);
    private PrintDocument print = new PrintDocument();
    private bool printing = false;

    public frmStruk(int no, DateTime datetime, string jenis)
    {
      InitializeComponent();
      txtKode.Text = Base36.Encode((int)datetime.TimeOfDay.TotalSeconds);
      txtTanggal.Text = datetime.Date.ToString("yyyy-MM-dd");
      txtJam.Text = datetime.ToString("HH:mm");
      txtJenis.Text = jenis;
      txtNoParkir.Text = no.ToString("0000");
    }

    private void struk_close(object sender, EventArgs e)
    {
      Owner.Show();
      Close();
    }

    private void printDocument_PrintPage(object sender, PrintPageEventArgs e) {
      Bitmap bitmap = new Bitmap(Size.Width, Size.Height, CreateGraphics());
      Graphics graphics = Graphics.FromImage(bitmap);
      graphics.CopyFromScreen(Location.X, Location.Y, 0, 0, Size);

      e.Graphics.DrawImage(bitmap, 0, 0);
    }

    private void print_form(object sender, EventArgs e) {
      if (!printing) {
        printing = !printing;
        printDocument.Print();
      }
    }
  }
}
