﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace parkir
{
    public partial class frmAwal : Form
    {
        public int Jumlah { get; set; }

        public int add_jumlah()
        {
            Jumlah += 1;
            return Jumlah;
        }

        public frmAwal()
        {
            InitializeComponent();
            Jumlah = 0;
        }

        private void pbxMobil_Click(object sender, EventArgs e)
        {
            var targetForm = new frmMasuk("Mobil");
            targetForm.Show(this);
            Hide();
        }

        private void pbxUp_Click(object sender, EventArgs e)
        {
            var frmKeluar = new frmPilih();
            frmKeluar.Show(this);
            Hide();
        }

        private void pbxMotor_Click(object sender, EventArgs e)
        {
            var targetForm = new frmMasuk("Motor");
            targetForm.Show(this);
            Hide();
        }

        private void pbxMobil_MouseHover(object sender, EventArgs e)
        {
            pbxMobil.Image = Properties.Resources.mobil_anim;
        }

        private void pbxMotor_MouseHover(object sender, EventArgs e)
        {
            pbxMotor.Image = Properties.Resources.motor_anim;
        }

        private void pbxMobil_MouseLeave(object sender, EventArgs e)
        {
            pbxMobil.Image = Properties.Resources.mobil;
        }

        private void pbxMotor_MouseLeave(object sender, EventArgs e)
        {
            pbxMotor.Image = Properties.Resources.motor;
        }
    }
}
