﻿namespace parkir
{
  partial class frmAwal
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.pbxUp = new System.Windows.Forms.PictureBox();
      this.pbxHelp = new System.Windows.Forms.PictureBox();
      this.pbxMotor = new RoundPictureBox();
      this.pbxMobil = new RoundPictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.pbxUp)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxHelp)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxMotor)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxMobil)).BeginInit();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.ForeColor = System.Drawing.Color.Teal;
      this.label1.Location = new System.Drawing.Point(13, 70);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(81, 13);
      this.label1.TabIndex = 9;
      this.label1.Text = "UPI PARKIR";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(137, 69);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(395, 31);
      this.label2.TabIndex = 10;
      this.label2.Text = "Selamat Datang di UPI Parkir";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(211, 322);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(55, 16);
      this.label3.TabIndex = 11;
      this.label3.Text = "MOBIL";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.Location = new System.Drawing.Point(351, 321);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(59, 16);
      this.label4.TabIndex = 12;
      this.label4.Text = "MOTOR";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label5.Location = new System.Drawing.Point(545, 409);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(43, 15);
      this.label5.TabIndex = 13;
      this.label5.Text = "HELP";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label6.Location = new System.Drawing.Point(175, 149);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(228, 14);
      this.label6.TabIndex = 14;
      this.label6.Text = "PILIH JENIS KENDARAAN ANDA :";
      // 
      // pbxUp
      // 
      this.pbxUp.Image = global::parkir.Properties.Resources.logo;
      this.pbxUp.Location = new System.Drawing.Point(27, 19);
      this.pbxUp.Name = "pbxUp";
      this.pbxUp.Size = new System.Drawing.Size(50, 50);
      this.pbxUp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pbxUp.TabIndex = 8;
      this.pbxUp.TabStop = false;
      this.pbxUp.Click += new System.EventHandler(this.pbxUp_Click);
      // 
      // pbxHelp
      // 
      this.pbxHelp.Image = global::parkir.Properties.Resources.question;
      this.pbxHelp.Location = new System.Drawing.Point(541, 357);
      this.pbxHelp.Name = "pbxHelp";
      this.pbxHelp.Size = new System.Drawing.Size(50, 50);
      this.pbxHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pbxHelp.TabIndex = 7;
      this.pbxHelp.TabStop = false;
      // 
      // pbxMotor
      // 
      this.pbxMotor.BackColor = System.Drawing.Color.Plum;
      this.pbxMotor.Image = global::parkir.Properties.Resources.motor;
      this.pbxMotor.Location = new System.Drawing.Point(319, 194);
      this.pbxMotor.Name = "pbxMotor";
      this.pbxMotor.Size = new System.Drawing.Size(120, 120);
      this.pbxMotor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pbxMotor.TabIndex = 6;
      this.pbxMotor.TabStop = false;
      this.pbxMotor.Click += new System.EventHandler(this.pbxMotor_Click);
      this.pbxMotor.MouseEnter += new System.EventHandler(this.pbxMotor_MouseHover);
      this.pbxMotor.MouseLeave += new System.EventHandler(this.pbxMotor_MouseLeave);
      // 
      // pbxMobil
      // 
      this.pbxMobil.BackColor = System.Drawing.Color.Plum;
      this.pbxMobil.Image = global::parkir.Properties.Resources.mobil;
      this.pbxMobil.Location = new System.Drawing.Point(177, 194);
      this.pbxMobil.Name = "pbxMobil";
      this.pbxMobil.Size = new System.Drawing.Size(120, 120);
      this.pbxMobil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pbxMobil.TabIndex = 5;
      this.pbxMobil.TabStop = false;
      this.pbxMobil.Click += new System.EventHandler(this.pbxMobil_Click);
      this.pbxMobil.MouseEnter += new System.EventHandler(this.pbxMobil_MouseHover);
      this.pbxMobil.MouseLeave += new System.EventHandler(this.pbxMobil_MouseLeave);
      // 
      // frmAwal
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Plum;
      this.ClientSize = new System.Drawing.Size(617, 444);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.pbxUp);
      this.Controls.Add(this.pbxHelp);
      this.Controls.Add(this.pbxMotor);
      this.Controls.Add(this.pbxMobil);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Name = "frmAwal";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "PARKIR";
      ((System.ComponentModel.ISupportInitialize)(this.pbxUp)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxHelp)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxMotor)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxMobil)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion
        private RoundPictureBox pbxMobil;
        private RoundPictureBox pbxMotor;
        private System.Windows.Forms.PictureBox pbxHelp;
        private System.Windows.Forms.PictureBox pbxUp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

