﻿namespace parkir
{
    partial class frmMasuk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.txtTanggal = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtJenisKendaraan = new System.Windows.Forms.TextBox();
            this.btnMasuk = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTarif = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtWaktu = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(115, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tanggal";
            // 
            // txtTanggal
            // 
            this.txtTanggal.Location = new System.Drawing.Point(167, 107);
            this.txtTanggal.Name = "txtTanggal";
            this.txtTanggal.ReadOnly = true;
            this.txtTanggal.Size = new System.Drawing.Size(100, 20);
            this.txtTanggal.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(103, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tarif Parkir";
            // 
            // txtJenisKendaraan
            // 
            this.txtJenisKendaraan.Location = new System.Drawing.Point(167, 159);
            this.txtJenisKendaraan.Name = "txtJenisKendaraan";
            this.txtJenisKendaraan.ReadOnly = true;
            this.txtJenisKendaraan.Size = new System.Drawing.Size(100, 20);
            this.txtJenisKendaraan.TabIndex = 5;
            // 
            // btnMasuk
            // 
            this.btnMasuk.Location = new System.Drawing.Point(273, 105);
            this.btnMasuk.Name = "btnMasuk";
            this.btnMasuk.Size = new System.Drawing.Size(75, 23);
            this.btnMasuk.TabIndex = 6;
            this.btnMasuk.Text = "Masuk";
            this.btnMasuk.UseVisualStyleBackColor = true;
            this.btnMasuk.Click += new System.EventHandler(this.btnMasuk_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(75, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Jenis Kendaraan";
            // 
            // txtTarif
            // 
            this.txtTarif.Location = new System.Drawing.Point(167, 185);
            this.txtTarif.Name = "txtTarif";
            this.txtTarif.ReadOnly = true;
            this.txtTarif.Size = new System.Drawing.Size(100, 20);
            this.txtTarif.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(122, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Waktu";
            // 
            // txtWaktu
            // 
            this.txtWaktu.Location = new System.Drawing.Point(167, 133);
            this.txtWaktu.Name = "txtWaktu";
            this.txtWaktu.ReadOnly = true;
            this.txtWaktu.Size = new System.Drawing.Size(100, 20);
            this.txtWaktu.TabIndex = 10;
            // 
            // frmMasuk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Plum;
            this.ClientSize = new System.Drawing.Size(475, 310);
            this.Controls.Add(this.txtWaktu);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtTarif);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnMasuk);
            this.Controls.Add(this.txtJenisKendaraan);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTanggal);
            this.Controls.Add(this.label2);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMasuk";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Selamat Datang";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.show_parent);
            this.Load += new System.EventHandler(this.frmMasuk_Load);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTanggal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtJenisKendaraan;
        private System.Windows.Forms.Button btnMasuk;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTarif;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtWaktu;
  }
}