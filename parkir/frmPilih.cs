﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace parkir
{
    public partial class frmPilih : Form
    {
        public frmPilih()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var targetForm = new frmLogin();
            targetForm.Show(this);
            Hide();
        }

        private void btnMasuk_Click(object sender, EventArgs e)
        {
            var targetForm = new frmAwal();
            targetForm.Show(this);
            Hide();
        }

        private void btnKeluar_Click(object sender, EventArgs e)
        {
            var targetForm = new frmKeluar();
            targetForm.Show(this);
            Hide();
        }
    }
}
