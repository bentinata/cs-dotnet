﻿namespace parkir
{
  partial class frmKeluar
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.txtKode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBiaya = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pbxUp = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxUp)).BeginInit();
            this.SuspendLayout();
            // 
            // txtKode
            // 
            this.txtKode.Font = new System.Drawing.Font("Consolas", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKode.Location = new System.Drawing.Point(39, 155);
            this.txtKode.Name = "txtKode";
            this.txtKode.Size = new System.Drawing.Size(260, 82);
            this.txtKode.TabIndex = 0;
            this.txtKode.Text = "KODE";
            this.txtKode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 139);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Kode";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 266);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Biaya";
            // 
            // txtBiaya
            // 
            this.txtBiaya.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBiaya.Location = new System.Drawing.Point(39, 282);
            this.txtBiaya.Name = "txtBiaya";
            this.txtBiaya.ReadOnly = true;
            this.txtBiaya.Size = new System.Drawing.Size(260, 44);
            this.txtBiaya.TabIndex = 3;
            this.txtBiaya.Text = "Rp5000";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(8, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "UPI PARKIR";
            // 
            // pbxUp
            // 
            this.pbxUp.Image = global::parkir.Properties.Resources.logo;
            this.pbxUp.Location = new System.Drawing.Point(22, 12);
            this.pbxUp.Name = "pbxUp";
            this.pbxUp.Size = new System.Drawing.Size(50, 50);
            this.pbxUp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxUp.TabIndex = 10;
            this.pbxUp.TabStop = false;
            // 
            // frmKeluar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Plum;
            this.ClientSize = new System.Drawing.Size(337, 372);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pbxUp);
            this.Controls.Add(this.txtBiaya);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtKode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmKeluar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Keluar";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmKeluar_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.pbxUp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
    }

    #endregion

    private System.Windows.Forms.TextBox txtKode;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox txtBiaya;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pbxUp;
    }
}